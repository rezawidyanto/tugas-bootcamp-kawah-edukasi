import java.util.Scanner;
import java.util.Arrays;

public class Tugas2 {
    public static void main(String[] args) {
        
        System.out.println(" "); 
        System.out.println("---------------------------------------------");
        System.out.println("| Menu Program Tugas 2");
        System.out.println("---------------------------------------------");
        System.out.println(" ");  
        System.out.println("1.Method Nilai Mutlak");
        System.out.println("2.Method Nilai Terbsar Di Array");
        System.out.println("3.Method Perpangkatan");
        System.out.println(" ");  
        
        Scanner scannerMenu = new Scanner(System.in);
        System.out.print("Pilih Menu Dengan Masukan Nomor Menu: ");

        if (scannerMenu.hasNextByte()) {

            byte angkaDariUser = scannerMenu.nextByte();

            switch (angkaDariUser) {

                case 1:

                 // Tampilan Menu Nilai Mutlak
                 Scanner scannerMutlak = new Scanner(System.in);
    
                 System.out.println(" ");
                 System.out.println("---------------------------------------------");    
                 System.out.println("| Method 1 Nilai Mutlak");
                 System.out.println("---------------------------------------------");
                 System.out.println(" ");

                 // Input Bilangan Yang Akan Dijadikan Nilai Mutlak
                 System.out.print("| Masukan Bilangan : ");

                 // Validasi Input Harus Angka
                 if (scannerMutlak.hasNextInt()) {

                    int nilaiMutlak = scannerMutlak.nextInt();
 
                    System.out.println(" ");  
                    System.out.println("---------------------------------------------");
         
                    // Hasil Nilai Mutlak
                    System.out.println("| Hasil = "+hasilMutlak(nilaiMutlak));
         
                    System.out.println("---------------------------------------------");
                    System.out.println(" ");      

                 } else {

                     // Pemberitahuan Jika Yang Di Input Pada Bilangan  Bukan Angka
                     System.out.println(" ");
                     System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                     System.out.println(" ");

                 }


                break;

                case 2:

                 // Tampilan Menu Perpangkatan
                 Scanner scannerNilaiTerbesar = new Scanner(System.in);
    
                 System.out.println(" ");
                 System.out.println("---------------------------------------------");    
                 System.out.println("| Method 2 Nilai Terbesar Di Array");
                 System.out.println("---------------------------------------------");
                 System.out.println(" ");

                 // Input Bilangan Untuk Dipangkatkan
                 System.out.print("Masukan Jumlah Nilai Array Yang Akan Di Input: ");

                 // Validasi Input Harus Angka
                 if (scannerNilaiTerbesar.hasNextInt()) {

                     int length = scannerNilaiTerbesar.nextInt();
                     int [] myArray = new int[length];

                     // Input Pangkat Dari Bilangan
                     System.out.println("Masukan Nilai Array : ");

                     // Validasi Input Harus Angka
                     if (scannerNilaiTerbesar.hasNextInt()) {

                        for( int i = 0; i < length; i++ ) {
                            myArray[i] = scannerNilaiTerbesar.nextInt();
                         }
 
                         System.out.println(" ");  
                         
                        // Nillai Array Yang Di input
                         System.out.println("---------------------------------------------"); 
                         System.out.println("Nilai Array yang Di Input"+Arrays.toString(myArray));
                         System.out.println("---------------------------------------------"); 

                         // Hasil Perpangkatan
                         System. out.println("Nilai Terbesar Di Array Tersebut Adalah : "+arrayTerbesar(myArray));
         
                         System.out.println("---------------------------------------------");
                         System.out.println(" ");      

                     } else {

                         // Pemberitahuan Jika Yang Di Input Pada Pangkat Bilangan Bukan Angka
                         System.out.println(" ");
                         System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                         System.out.println(" ");
                           
                     }
                     
                 } else {

                     // Pemberitahuan Jika Yang Di Input Pada Bilangan  Bukan Angka
                     System.out.println(" ");
                     System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                     System.out.println(" ");

                 }
                
                break;


                case 3:

                 // Tampilan Menu Perpangkatan
                 Scanner scannerPangkat = new Scanner(System.in);
    
                 System.out.println(" ");
                 System.out.println("---------------------------------------------");    
                 System.out.println("| Method 3 Perpangkatan");
                 System.out.println("---------------------------------------------");
                 System.out.println(" ");

                 // Input Bilangan Untuk Dipangkatkan
                 System.out.print("| Masukan Bilangan : ");

                 // Validasi Input Harus Angka
                 if (scannerPangkat.hasNextInt()) {

                     int bilangan = scannerPangkat.nextInt();

                     // Input Pangkat Dari Bilangan
                     System.out.print("| Masukan Pangkat Bilangan : ");

                     // Validasi Input Harus Angka
                     if (scannerPangkat.hasNextInt()) {

                        int pangkat= scannerPangkat.nextInt();
 
                         System.out.println(" ");  
                         System.out.println("---------------------------------------------");
         
                         // Hasil Perpangkatan
                         System.out.println("| Hasil "+bilangan+" pangkat "+pangkat+" = "+bilanganPangkat(bilangan, pangkat));
         
                         System.out.println("---------------------------------------------");
                         System.out.println(" ");      

                     } else {

                         // Pemberitahuan Jika Yang Di Input Pada Pangkat Bilangan Bukan Angka
                         System.out.println(" ");
                         System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                         System.out.println(" ");
                           
                     }
                     
                 } else {

                     // Pemberitahuan Jika Yang Di Input Pada Bilangan  Bukan Angka
                     System.out.println(" ");
                     System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                     System.out.println(" ");

                 }

                break;

                default:

                System.out.println(" ");
                System.out.println("Harap masukan nomor menu sesuai menu diatas");
                System.out.println(" ");

            }
    
            scannerMenu.close();    
    
        } else {

            System.out.println(" ");
            System.out.println("Hanya menerima inputan nomor menu");
            System.out.println(" ");

        }

    }

    
    // Method Perpangkatan
    public static int bilanganPangkat(int bilangan, int pangkat) {
        
        int hasil = 1;
        
        for(int i = 1 ; i <= pangkat; i++){

           hasil *= bilangan; 

        }

        return hasil;
        
    }

    // Method Array Terbesar
    public static int arrayTerbesar(int [] myArray) {
      
        int indexTerbesar2 = myArray[0];

        for(int ii = 0; ii < myArray.length; ii++){

            if(indexTerbesar2 < myArray[ii]){

                indexTerbesar2 = myArray[ii];

            }

        }

        return indexTerbesar2;

    }

    // Method Nilai Mutlak
    public static int hasilMutlak(int nilaiMutlak) {

        if (nilaiMutlak < 0) {

            nilaiMutlak = -nilaiMutlak;

            return nilaiMutlak;

        } else {
                    
            return nilaiMutlak;
              
        }

    }
}
