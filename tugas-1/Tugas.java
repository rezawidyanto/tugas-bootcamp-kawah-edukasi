import java.util.Scanner;

public class Tugas {
    public static void main(String[] args) {
        
        // Tampilan Pilihan Menu Awal
        System.out.println(" "); 
        System.out.println("---------------------------------------------");
        System.out.println("| Menu Program Tugas 1");
        System.out.println("---------------------------------------------");
        System.out.println(" ");  
        System.out.println("1.Data Diri");
        System.out.println("2.Kalkulator");
        System.out.println("3.Perbandingan");
        System.out.println(" ");  
        
        // Input Pilihan Menu Menggunakan Nomor 
        Scanner scannerMenu = new Scanner(System.in);
        System.out.print("Pilih Menu Dengan Masukan Nomor Menu: ");

        // Validasi Input Harus Angka
        if (scannerMenu.hasNextByte()) {

            byte angkaDariUser = scannerMenu.nextByte();

            // Perkondisian Sesuai Nomor Menu Yang Dipilih
            switch (angkaDariUser) {

                case 1:
    
                    // Tampilan Menu Data Diri
                    System.out.println(" ");
                    System.out.println("---------------------------------------------");    
                    System.out.println("| Menu Data Diri");
                    System.out.println("---------------------------------------------");
                    System.out.println(" ");
                    System.out.println("1.Nama : Reza Widyanto");
                    System.out.println(" ");
                    System.out.println("2.Alasan kenapa menjadi seorang back-end developer :");
                    System.out.println(" ");
                    System.out.println("Karena memang saya kurang di bidang tampilan atau front end akhirnya saya sadar dengan diri saya dan memilih back end, memang dari dulu sangat suka di bagian sistem nya namun tampilan nya gak bagus, yang penting semua proses berjalan dengan baik hehe");
                    System.out.println(" ");
                    System.out.println("3.Ekspektasi saya dengan mengikuti bootcamp ini :");
                    System.out.println(" ");
                    System.out.println("Pada saat saya tahu adanya Kawah Edukasi saya seperti mendapatkan titik trang dari apa yang saya bingungkan selama ini, dengan adanya program penyaluran kerja setelah berakhirnya platihan, itu membuat rasa takut & ragu saya dalam dunia programing dapat teratasi, apalagi dengan pelatihan yang di dasari dengan dunia profesional itu membuat saya semakin percaya diri untuk berkarir di dunia programming, tinggal bagaimana saya menghadapi & memanfaatkan kesempatan ini sebaik baik nya, sekarang saya memiliki tujuan & mimpi ingin menjadi Programmer Profesional yang bisa membuat aplikasi yang memudahkan kita semua, Terima Kasih Kawah Edukasi");
                    System.out.println(" ");
    
                break;

                case 2:

                    // Tampilan Menu Kalkulator beserta Pilihan Menu Kalkulator
                    System.out.println(" ");
                    System.out.println("---------------------------------------------");    
                    System.out.println("| Menu Kalkulator");
                    System.out.println("---------------------------------------------");
                    System.out.println(" ");
                    System.out.println("1.Penjumlahan");
                    System.out.println("2.Pengurangan");
                    System.out.println("3.Perkalian");
                    System.out.println("4.Pembagian");
                    System.out.println("5.Sisa Bagi");
                    System.out.println(" ");    
                    
                    // Input Pilihan Menu Kalkulator Menggunakan Nomor Menu Kalkulator
                    Scanner scannerKalkulator = new Scanner(System.in);
                    System.out.print("Pilih Jenis Operasi Dengan Masukan Nomor Jenis Operasi: ");

                    // Validasi Input Harus Angka
                    if (scannerKalkulator.hasNextByte()) {

                        byte operasiDariUser = scannerKalkulator.nextByte();

                        // Perkondisian Sesuai Nomor Menu Kalkulator Yang Dipilih
                        switch (operasiDariUser) {
                            
                            case 1:

                                // Tampilan Menu Penjumlahan
                                Scanner scannerTambah = new Scanner(System.in);
                                
                                System.out.println(" ");
                                System.out.println("---------------------------------------------");
                                System.out.println("| Program Untuk Operasi Penjumlahan ");
                                System.out.println("---------------------------------------------");
                                System.out.println(" ");  

                                // Input Bilangan A Untuk Penjumlahan
                                System.out.print("| Masukan Nilai A : ");

                                // Validasi Input Harus Angka
                                if (scannerTambah.hasNextInt()) {

                                    int bilanganATambah = scannerTambah.nextInt();
                                    
                                    // Input Bilangan B Untuk Penjumlahan
                                    System.out.print("| Masukan Nilai B : ");

                                    // Validasi Input Harus Angka
                                    if (scannerTambah.hasNextInt()) {

                                        int bilanganBTambah = scannerTambah.nextInt();

                                        // Proses Operasi Penjumlahan
                                        int hasilPenjumlahan = bilanganATambah + bilanganBTambah;

                                        // Tampilan Hasil Penjumlahan
                                        System.out.println(" ");  
                                        System.out.println("---------------------------------------------");
                                        System.out.println("| Hasil Dari Penjumlahan A + B Adalah = " + hasilPenjumlahan);
                                        System.out.println("---------------------------------------------");
                                        System.out.println(" ");  

                                        scannerTambah.close();  
                                        
                                    } else {

                                        // Pemberitahuan Jika Yang Di Input Pada Bilangan B Bukan Angka
                                        System.out.println(" ");
                                        System.out.println("Hanya Menerima Imputan Bilangan Bulat");  

                                    }

                                } else {

                                    // Pemberitahuan Jika Yang Di Input Pada Bilangan A Bukan Angka
                                    System.out.println(" ");
                                    System.out.println("Hanya Menerima Imputan Bilangan Bulat");

                                }
        
                            break;
        
                            case 2:  

                                // Tampilan Menu Pengurangan
                                Scanner scannerKurang = new Scanner(System.in);
        
                                System.out.println(" ");
                                System.out.println("---------------------------------------------");
                                System.out.println("| Program Untuk Operasi Pengurangan ");
                                System.out.println("---------------------------------------------");
                                System.out.println(" ");  
                                
                                // Input Bilangan A Untuk Pengurangan
                                System.out.print("| Masukan Nilai A : ");

                                // Validasi Input Harus Angka
                                if (scannerKurang.hasNextInt()) {

                                    int bilanganAKurang = scannerKurang.nextInt();

                                    // Input Bilangan B Untuk Pengurangan
                                    System.out.print("| Masukan Nilai B : ");

                                    // Validasi Input Harus Angka
                                    if (scannerKurang.hasNextInt()) {

                                        int bilanganBKurang = scannerKurang.nextInt();

                                        // Proses Operasi Pengurangan
                                        int hasilPengurangan = bilanganAKurang - bilanganBKurang;
                                        
                                        // Tampilan Hasil Pengurangan
                                        System.out.println(" ");  
                                        System.out.println("---------------------------------------------");
                                        System.out.println("| Hasil Dari Pengurangan A - B Adalah = " + hasilPengurangan);
                                        System.out.println("---------------------------------------------");
                                        System.out.println(" "); 
                                        
                                        scannerKurang.close(); 

                                    } else {

                                        // Pemberitahuan Jika Yang Di Input Pada Bilangan B Bukan Angka
                                        System.out.println(" ");
                                        System.out.println("Hanya Menerima Imputan Bilangan Bulat");  

                                    }

                                } else {

                                    // Pemberitahuan Jika Yang Di Input Pada Bilangan A Bukan Angka
                                    System.out.println(" ");
                                    System.out.println("Hanya Menerima Imputan Bilangan Bulat");

                                }
        
                            break;
        
                            case 3:

                                // Tampilan Menu Perkalian
                                Scanner scannerKali = new Scanner(System.in);
        
                                System.out.println(" ");
                                System.out.println("---------------------------------------------");
                                System.out.println("| Program Untuk Operasi Perkalian ");
                                System.out.println("---------------------------------------------");
                                System.out.println(" ");  

                                // Input Bilangan A Untuk Perkalian
                                System.out.print("| Masukan Nilai A : ");

                                // Validasi Input Harus Angka
                                if (scannerKali.hasNextInt()) {
                                    
                                    int bilanganAKali = scannerKali.nextInt();

                                    // Input Bilangan B Untuk Perkalian
                                    System.out.print("| Masukan Nilai B : ");

                                    // Validasi Input Harus Angka
                                    if (scannerKali.hasNextInt()) {

                                        int bilanganBKali = scannerKali.nextInt();

                                        // Proses Operasi Perkalian
                                        int hasilPerkalian = bilanganAKali * bilanganBKali;
        
                                        // Tampilan Hasil Perkalian
                                        System.out.println(" ");  
                                        System.out.println("---------------------------------------------");
                                        System.out.println("| Hasil Dari Perkalian A X B Adalah = " + hasilPerkalian);
                                        System.out.println("---------------------------------------------");
                                        System.out.println(" ");  
        
                                        scannerKali.close(); 

                                    } else {

                                        // Pemberitahuan Jika Yang Di Input Pada Bilangan B Bukan Angka
                                        System.out.println(" ");
                                        System.out.println("Hanya Menerima Imputan Bilangan Bulat");  

                                    }
  
                                } else {

                                    // Pemberitahuan Jika Yang Di Input Pada Bilangan A Bukan Angka
                                    System.out.println(" ");
                                    System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                                }
        
                            break;
        
                            case 4:
                            
                                // Tampilan Menu Pembagian
                                Scanner scannerBagi = new Scanner(System.in);
        
                                System.out.println(" ");
                                System.out.println("---------------------------------------------");
                                System.out.println("| Program Untuk Operasi Pembagian ");
                                System.out.println("---------------------------------------------");
                                System.out.println(" ");  
        
                                // Input Bilangan A Untuk pembagian
                                System.out.print("| Masukan Nilai A : ");

                                // Validasi Input Harus Angka
                                if (scannerBagi.hasNextInt()) {

                                    int bilanganABagi = scannerBagi.nextInt();

                                    // Input Bilangan B Untuk Pembagian
                                    System.out.print("| Masukan Nilai B : ");

                                    // Validasi Input Harus Angka
                                    if (scannerBagi.hasNextInt()) {

                                        int bilanganBBagi = scannerBagi.nextInt();

                                        // Proses Operasi Pembagian
                                        int hasilPembagian = bilanganABagi / bilanganBBagi;
        
                                        // Tampilan Hasil Pembagian
                                        System.out.println(" ");  
                                        System.out.println("---------------------------------------------");
                                        System.out.println("| Hasil Dari Pembagian A / B Adalah = " + hasilPembagian);
                                        System.out.println("---------------------------------------------");
                                        System.out.println(" ");  
        
                                        scannerBagi.close(); 

                                    } else {

                                        // Pemberitahuan Jika Yang Di Input Pada Bilangan B Bukan Angka
                                        System.out.println(" ");
                                        System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                                          
                                    }
                                    
                                } else {

                                    // Pemberitahuan Jika Yang Di Input Pada Bilangan A Bukan Angka
                                    System.out.println(" ");
                                    System.out.println("Hanya Menerima Imputan Bilangan Bulat");

                                }
        
                            break;
        
                            case 5:
                            
                                // Tampilan Menu Sisa Pembagian
                                Scanner scannerSisaBagi = new Scanner(System.in);
        
                                System.out.println(" ");
                                System.out.println("---------------------------------------------");
                                System.out.println("| Program Untuk Operasi Sisa Bagi ");
                                System.out.println("---------------------------------------------");
                                System.out.println(" ");  
        
                                // Input Bilangan A Untuk Sisa Pembagian
                                System.out.print("| Masukan Nilai A : ");

                                // Validasi Input Harus Angka
                                if (scannerSisaBagi.hasNextInt()) {

                                    int bilanganASisaBagi= scannerSisaBagi.nextInt();

                                    // Input Bilangan B Untuk Sisa Pembagian
                                    System.out.print("| Masukan Nilai B : ");

                                    // Validasi Input Harus Angka
                                    if (scannerSisaBagi.hasNextInt()) {

                                        int bilanganBSisaBagi= scannerSisaBagi.nextInt();

                                        // Proses Operasi Sisa Pembagian
                                        int hasilSisaBagi = bilanganASisaBagi % bilanganBSisaBagi;
                
                                        // Tampilan Hasil Pembagian
                                        System.out.println(" ");  
                                        System.out.println("---------------------------------------------");
                                        System.out.println("| Hasil Dari Sisa Bagi A / B Adalah = " + hasilSisaBagi);
                                        System.out.println("---------------------------------------------");
                                        System.out.println(" ");

                                        scannerSisaBagi.close(); 

                                    } else {

                                        // Pemberitahuan Jika Yang Di Input Pada Bilangan B Bukan Angka
                                        System.out.println(" ");
                                        System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                                          
                                    }

                                } else {

                                    // Pemberitahuan Jika Yang Di Input Pada Bilangan A Bukan Angka
                                    System.out.println(" ");
                                    System.out.println("Hanya Menerima Imputan Bilangan Bulat");

                                }
        
                            break;
                        
                            default:
                            
                                // Pemberitahuan Jika Nomor Menu Kalkulator Yang Di Input Tidak Sesuai Dengan Menu Yang Ada
                                System.out.println("Harap masukan nomor menu kalkulator sesuai menu diatas");

                            break;

                        }

                        scannerKalkulator.close();    

                    } else {

                        // Pemberitahuan Jika Yang Di Input Bukan Nomor Menu Kalkulator
                        System.out.println(" ");
                        System.out.println("Hanya menerima inputan nomor menu Kalkulator");

                    }
    
                break;
    
                case 3:

                    // Tampilan Menu Perbandingan
                    Scanner scannerPerbandingan = new Scanner(System.in);
    
                    System.out.println(" ");
                    System.out.println("---------------------------------------------");    
                    System.out.println("| Menu Perbandingan");
                    System.out.println("---------------------------------------------");
                    System.out.println(" ");  
                    System.out.println("| Program Untuk Operasi Perbandingan ");
                    System.out.println(" ");  

                    // Input Bilangan A Untuk Perbandingan
                    System.out.print("| Masukan Nilai A : ");

                    // Validasi Input Harus Angka
                    if (scannerPerbandingan.hasNextInt()) {

                        int bilanganAPerbandingan = scannerPerbandingan.nextInt();

                        // Input Bilangan B Untuk Perbandingan
                        System.out.print("| Masukan Nilai B : ");

                        // Validasi Input Harus Angka
                        if (scannerPerbandingan.hasNextInt()) {

                            int bilanganBPerbandingan = scannerPerbandingan.nextInt();
    
                            System.out.println(" ");  
                            System.out.println("---------------------------------------------");
            
                            // Pengkondisian Berdasarkan Dengan Inputan Bilangan A & B 
                            if (bilanganAPerbandingan < bilanganBPerbandingan) {
            
                                System.out.print("Bilangan A : " + bilanganAPerbandingan);
                                System.out.print( " Lebih Kecil Dari " );
                                System.out.println("Bilangan B : " + bilanganBPerbandingan);
                                
                            } else if (bilanganAPerbandingan == bilanganBPerbandingan) {
            
                                System.out.print("Bilangan A : " + bilanganAPerbandingan);
                                System.out.print( " Sama Dengan " );
                                System.out.println("Bilangan B : " + bilanganBPerbandingan);
            
                            } else {
            
                                System.out.print("Bilangan A : " + bilanganAPerbandingan);
                                System.out.print( " Lebih Besar Dari " );
                                System.out.println("Bilangan B : " + bilanganBPerbandingan);
            
                            }
            
                            System.out.println("---------------------------------------------");
                            System.out.println(" ");      

                        } else {

                            // Pemberitahuan Jika Yang Di Input Pada Bilangan B Bukan Angka
                            System.out.println(" ");
                            System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                            System.out.println(" ");
                              
                        }
                        
                    } else {

                        // Pemberitahuan Jika Yang Di Input Pada Bilangan A Bukan Angka
                        System.out.println(" ");
                        System.out.println("Hanya Menerima Imputan Bilangan Bulat");
                        System.out.println(" ");

                    }

                break;
                default:

                // Pemberitahuan Jika Nomor Menu Yang Di Input Tidak Sesuai Dengan Menu Yang Ada
                System.out.println(" ");
                System.out.println("Harap masukan nomor menu sesuai menu diatas");
                System.out.println(" ");
            }
    
            scannerMenu.close();    
    
        } else {

            // Pemberitahuan Jika Yang Di Input Bukan Nomor Menu
            System.out.println(" ");
            System.out.println("Hanya menerima inputan nomor menu");
            System.out.println(" ");

        }

    }
}
