package tech.mahrez.models;

import javax.validation.constraints.NotBlank;

public class Tool {

    
    @NotBlank(message="Nama Tidak Boleh Kosong")
    public String name;

    @NotBlank(message="Warna Tidak Boleh Kosong")
    public String colour;

    @NotBlank(message="Tempat Tidak Boleh Kosong")
    public String savedin;

    @NotBlank(message="Jumlah Tidak Boleh Kosong")
    public Integer total;

    public Tool() {
    }

    public Tool(String name, String colour, String savedin, Integer total) {
        this.name = name;
        this.colour = colour;
        this.savedin = savedin;
        this.total = total;
    }
}
