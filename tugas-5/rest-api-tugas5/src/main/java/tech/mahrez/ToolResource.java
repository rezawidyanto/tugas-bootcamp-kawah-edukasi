package tech.mahrez;

import tech.mahrez.models.Tool;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Path("tools")
public class ToolResource {

    @Inject
    Validator validator;

    List<Tool> tools = new ArrayList<>();

    @GET
    public List<Tool> gettools() {
        return tools;
    }

    @GET
    @Path("{index}")
    public Tool getToolByIndex(@PathParam("index") Integer index) {
        return tools.get(index);
    }

    // @POST
    // public Result addTool(Tool newTool) {
    //     Set<ConstraintViolation<Tool>> violations =
    //     validator.validate(newTool);
    //     if (violations.isEmpty()) {
    //         tools.add(newTool);
    //         return new Result("Buah baru berhasil ditambahkan");
    //     } else {
    //         return new Result(violations);
    //     }
    
    // }

    @POST
    public Result addTool(@Valid Tool newTool) {
        return new Result("Tool baru berhasil ditambahkan");
    }


    @PUT
    @Path("{index}")
    public Map<String, Tool> changeTool(@PathParam("index") Integer index, Tool newTool) {
        Map<String, Tool> result = new HashMap<>();
        result.put("oldTool", tools.get(index));
        result.put("newTool", newTool);
        tools.set(index, newTool);
        return result;
    }

    @DELETE
    @Path("{index}")
    public Object deleteTool(@PathParam("index") Integer index) {
        try {
            Tool Tool = tools.get(index);
            tools.remove(index.intValue());
            return Tool;
        } catch (Exception e) {
            return "data anda tidak ada";
        }
    }

    public static class Result {
	
    private String message;
    private boolean success;

    Result(String message) {
        this.success = true;
        this.message = message;
    }
	
    Result(Set<? extends ConstraintViolation<?>> violations) {
        this.success = false;
        this.message = violations.stream()
                .map(cv -> cv.getMessage())
                .collect(Collectors.joining(", "));
    }
	
    public String getMessage() {
        return message;
    }
	
    public boolean isSuccess() {
        return success;
    }
	
    }

}
